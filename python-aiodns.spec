%global srcname aiodns

Summary:        Simple DNS resolver for asyncio
Name:           python-%{srcname}
Version:        3.1.1
Release:        3%{?dist}
License:        MIT
URL:            https://github.com/saghul/%{srcname}
Source0:        https://github.com/saghul/aiodns/archive/refs/tags/v%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel python3-setuptools python3-pycares

%description
aiodns provides a simple way for doing asynchronous DNS resolutions
with a synchronous looking interface by using pycares.


%package     -n python3-%{srcname}
Summary:        Simple DNS resolver for asyncio
BuildArch:      noarch
Requires:       python3-pycares

%description -n python3-%{srcname}
aiodns provides a simple way for doing asynchronous DNS resolutions
with a synchronous looking interface by using pycares.



%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build


%install
%py3_install


%check


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst ChangeLog
%{python3_sitelib}/%{srcname}-%{version}-py%{python3_version}.egg-info/
%{python3_sitelib}/%{srcname}/


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.1.1-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.1.1-2
- Rebuilt for loongarch release

* Wed Jan 03 2024 Upgrade Robot <upbot@opencloudos.org> - 3.1.1-1
- Upgrade to version 3.1.1

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.0.0-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.0.0-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.0.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.0.0-2
- Rebuilt for OpenCloudOS Stream 23

* Wed Mar 15 2023 rockerzhu <rockerzhu@tencent.com> - 3.0.0-1
- Initial build
